from flask import Flask, request, jsonify
import pandas as pd
import prophet_model  # Импорт вашего скрипта, назовем его prophet.py

app = Flask(__name__)

@app.route('/upload', methods=['POST'])
def upload_file():
    # Проверяем, есть ли файл в запросе
    if 'file' not in request.files:
        return "No file part in the request", 400
    file = request.files['file']

    # Если пользователь не выбрал файл, браузер отправит
    # пустую часть без имени файла
    if file.filename == '':
        return "No selected file", 400

    if file:
        # Обработка файла через prophet_model
        data = prophet_model.main_process(file)

        # Отправляем обработанные данные обратно как JSON
        return jsonify(data.to_dict(orient='records')), 200


if __name__ == '__main__':
    app.run(debug=True)
