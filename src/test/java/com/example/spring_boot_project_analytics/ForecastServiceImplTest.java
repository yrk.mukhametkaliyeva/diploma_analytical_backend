package com.example.spring_boot_project_analytics;


import com.example.spring_boot_project_analytics.MainTable.Helper.JsonUtil;
import com.example.spring_boot_project_analytics.MainTable.dto.RecordDto;
import com.example.spring_boot_project_analytics.TableDetails.entity.TableDetails;
import com.example.spring_boot_project_analytics.Forecast.repository.ForecastResultRepository;
import com.example.spring_boot_project_analytics.TableDetails.repository.TableDetailsRepository;
import com.example.spring_boot_project_analytics.Forecast.service.impl.ForecastServiceImpl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ForecastServiceImplTest {

    private MockWebServer mockWebServer;
    private ForecastServiceImpl service;

    @Mock
    private TableDetailsRepository tableDetailsRepository;
    @Mock
    private ForecastResultRepository forecastResultRepository;
    private WebClient webClient;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        this.webClient = WebClient.builder()
                .baseUrl(mockWebServer.url("/").toString())
                .build();
        this.service = new ForecastServiceImpl(WebClient.builder(), tableDetailsRepository, forecastResultRepository);
    }

    @AfterEach
    void tearDown() throws Exception {
        mockWebServer.shutdown();
    }

    @Test
    void sendFileToFlaskAndGetForecastAsync_sendsCorrectJsonAndProcessesResults() throws Exception {
        // Arrange
        Long metaId = 6L;
        List<RecordDto> fakeRecordDtos = createFakeRecordDtos(); // Set up your TableDetails with records
        TableDetails table = new TableDetails();
        when(tableDetailsRepository.findById(metaId)).thenReturn(java.util.Optional.of(table));


        String expectedJson = JsonUtil.toJson(fakeRecordDtos);
        mockWebServer.enqueue(new MockResponse().setBody("[{\"id\": 1}]").setResponseCode(200));

        // Act
        service.sendFileToFlaskAndGetForecastAsync(metaId);

        // Assert
        var recordedRequest = mockWebServer.takeRequest();
        assertEquals("POST", recordedRequest.getMethod());
        assertEquals(expectedJson, recordedRequest.getBody().readUtf8());
        assertEquals("/upload-flask", recordedRequest.getPath());

        // Verify interactions
        verify(tableDetailsRepository).findById(metaId);
        verify(forecastResultRepository).saveAll(any());
    }

    public static List<RecordDto> createFakeRecordDtos() {
        return Arrays.asList(
                new RecordDto(1L, 101, "2021-06", 1200.00, 10, 50, 100.0, 5.0, 20, "Electronics"),
                new RecordDto(2L, 102, "2021-07", 1500.00, 5, 30, 150.0, 2.0, 30, "Clothing"),
                new RecordDto(3L, 103, "2021-08", 1600.00, 8, 40, 120.0, 3.0, 40, "Furniture")
        );
    }
}
